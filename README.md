Divano is a reputed furniture store having its showroom on Dundas Street (W), Mississauga, co-founded by Awn Al-Moman and Ihsan Aljammali in June 2015. Our wide array of furnishings, curtains and home dÃ©cor accessories combine elegance with warmth that surpasses your expectations. Our inventory comprises of sofas, coffee tables, dining tables, crystal chandeliers, Turkish furniture and home decor accessories. We provide exquisite workmanship and excellent customer support. We accept payments by cash, Debit Cards and Credit Cards.

Website: https://www.divano.ca/furniture-store-mississauga/
